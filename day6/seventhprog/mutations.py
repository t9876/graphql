from ariadne import convert_kwargs_to_snake_case, MutationType
from store import messages, queues


mutation = MutationType()


@mutation.field("createMessage")
@convert_kwargs_to_snake_case
async def resolve_create_message(*_, content, sender_id, recipient_id):
    try:
        message = {
            "content": content,
            "sender_id": sender_id,
            "recipient_id": recipient_id
        }
        messages.append(message)
        for queue in queues:
            await queue.put(message)
        return {
            "success": True,
            "message": message
        }
    except Exception as error:
        return {
            "success": False,
            "errors": [str(error)]
        }


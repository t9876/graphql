from datetime import datetime


suppliers = [
    {
        'id': '1',
        'name': 'Milk Supplier',
        'address': '77 Milk Way',
        'contactNumber': '0987654321',
        'email': 'milk@milksupplier.com',
    },
]

ingredients = [
    {
        'id': '1',
        'name': 'Milk',
        'stock': {
            'quantity': 100.00,
            'unit': 'LITERS',
        },
        'products': [],
        'supplier': '1',
        'description': 'Liquid',
        'lastUpdated': datetime.utcnow(),
    },
]


products = [
    {
        'id': '1',
        'name': 'Walnut Bomb',
        'price': 37.00,
        'size': 'MEDIUM',
        'available': False,
        'ingredients': [
            {
                'ingredient': '1',
                'quantity': 100.00,
                'unit': 'LITERS',
            }
        ],
        'hasFilling': False,
        'hasNutsToppingOption': True,
        'lastUpdated': datetime.utcnow(),
    },
    {
        'id': '2',
        'name': 'Cappuccino Star',
        'price': 12.50,
        'size': 'SMALL',
        'available': True,
        'ingredients': [
            {
                'ingredient': '1',
                'quantity': 100.00,
                'unit': 'LITERS',
            }
        ],
        'hasCreamOnTopOption': True,
        'hasServeOnIceOption': True,
        'lastUpdated': datetime.utcnow(),
    },
]
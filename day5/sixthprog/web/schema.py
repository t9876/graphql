from pathlib import Path

from ariadne import make_executable_schema, format_error
from exceptions import ProductNotFoundError

from web.mutations import mutation
from web.queries import query
from web.types import product_type, datetime_scalar, product_interface, ingredient_type, supplier_type


def my_format_error(error: ProductNotFoundError, debug: bool = False) -> dict:
    if debug:
        # If debug is enabled, reuse Ariadne's formatting logic (not required)
        return format_error(error, debug)

    # Create formatted error data
    formatted = error.formatted
    # Replace original error message with custom one
    formatted["message"] = "PRODUCT NOT FOUND"
    return formatted

schema = make_executable_schema(
    (Path(__file__).parent / 'products.graphql').read_text(),
    [query, mutation, product_interface, product_type, ingredient_type, supplier_type, datetime_scalar]
)
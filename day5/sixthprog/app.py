from ariadne.asgi import GraphQL
from web.schema import my_format_error
from web.schema import schema
 
app = GraphQL(schema, error_formatter=my_format_error, debug=True)
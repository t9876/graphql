class ProductNotFoundError(Exception):
    extensions = {"code": " PRODUCT NOT FOUND"}

    @staticmethod
    def error_handler():
        return Exception

class IngredientNotFoundError(Exception):
    extensions = {"code": " INGREDIENT NOT FOUND"}

    @staticmethod
    def error_handler():
        return Exception


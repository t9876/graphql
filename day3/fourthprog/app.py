from ariadne.asgi import GraphQL
from web.schema import schema
 
app = GraphQL(schema, debug=True)
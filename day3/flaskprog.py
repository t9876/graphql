from ariadne import ObjectType, QueryType, gql, make_executable_schema, graphql_sync
#from ariadne.asgi import GraphQL
from flask import Flask, request, jsonify


app = Flask(__name__)

type_defs = gql("""
    type Query {
        people: [Person!]!
    }

    type Person {
        firstName: String!
        lastName: String!
        age: Int!
        fullName: String!
    }
""")

query = QueryType()

@query.field("people")
def resolve_people(*_):
    print(*_)
    return [
        {"firstName": "John", "lastName": "Doe", "age": 21},
        {"firstName": "Bob", "lastName": "Boberson", "age": 24},
    ]

person = ObjectType("Person")

@person.field("fullName")
def resolve_person_fullname(person, *_):
    print(person)
    return "%s %s" % (person["firstName"], person["lastName"])


schema = make_executable_schema(type_defs, query, person)

#app = GraphQL(schema, debug=True)
# @app.route('/graphql', methods=['GET'])
# def playground():
#     return PLAYGROUND_HTML, 200
    
@app.route('/graphql', methods=['POST'])
def graphql_server():
    data = request.get_json()
    success, result = graphql_sync(
    schema,
    data,
    context_value=None,
    debug=app.debug
    )
    status_code = 200 if success else 400
    return jsonify(result), status_code
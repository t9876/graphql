from ariadne.asgi import GraphQL
from ariadne_graphql_modules import ObjectType, gql, make_executable_schema, DeferredType


class PersonType(ObjectType):
    __schema__ = gql(
        """
        type Person {
            firstname: String!
            lastname: String!
            age: Int!
            fullname: String!
        }""")

    @staticmethod
    def resolve_fullname(Person, *_):
       return "%s %s" % (Person["firstname"], Person["lastname"])

class NameQuery(ObjectType):
    __schema__ = gql(
        """
        type Query{
            people : [Person!]
        }""")

    __requires__ = [DeferredType("Person")]

    @staticmethod
    def resolve_people(*_):
        return [
        { "firstname": "Sourav", "lastname": "Roy", "age": 31},
        { "firstname": "Pratik", "lastname": "Kumar", "age": 24}
    ]



# Binding schema and making it executable
schema = make_executable_schema(NameQuery, PersonType)
app = GraphQL(schema, debug=True)
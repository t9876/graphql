## graphql

This repo is created as part of training program on graphql

## Installing Requirements
Use Virtualenv and install the packages.
```
python -m venv venv_day1
source venv_day1/bin/activate
```
```
pip install -r requirements.txt
```
## Running uvicorn Server
Go to the root dir and run the below line in the terminal.
```
uvicorn firstprog:app
```

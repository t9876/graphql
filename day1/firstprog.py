from ariadne import QueryType, gql, make_executable_schema, ObjectType
from ariadne.asgi import GraphQL


#*************Client side*************************
# Schema Definition & Validation using gql
type_def = gql("""
    type Query{
        hello: String!
    }""")


#****************** Server side logic**************************
# Instantiating query as QueryType object
#query = QueryType()
query = ObjectType("Query")


# Resolver function to resolve field "hello"
@query.field("hello")
#def resolve_hello(*_):
def resolve_hello(_, info):
    print(info)
    request = info.context["request"]
    print(request.headers)
    user = request.headers.get("user-agent")
    return "Welcome to GraphQL playground, %s" % user


#Binding schema and making it executable
schema = make_executable_schema(type_def, query)
app = GraphQL(schema,debug=True)

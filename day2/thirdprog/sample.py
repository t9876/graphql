from ariadne import make_executable_schema, load_schema_from_path, ObjectType, QueryType
from ariadne.asgi import GraphQL
import resolvers as r


type_defs = load_schema_from_path('schema.graphql')

query = QueryType()
building = ObjectType('Building')
resident = ObjectType('Resident')

query.set_field('building_with_id', r.building_with_id)
building.set_field('residents', r.resolve_residents_in_building)
query.set_field('resident_with_id', r.resident_with_id)

schema = make_executable_schema(type_defs, [building, resident, query])
app = GraphQL(schema, debug=True)
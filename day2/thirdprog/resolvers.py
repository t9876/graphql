import json

def building_with_id(*_, id):
    with open('./buildings.json') as file:
        data = json.load(file)
        for building in data['buildings']:
            if building['id'] == id:
                return building

def resolve_residents_in_building(building, *_):
    with open('./residents.json') as file:
        data = json.load(file)
        residents = [
            resident 
            for resident in data['residents'] 
                if resident['building'] == building['id']]
        return residents

def resident_with_id(*_, id):
    with open('./residents.json') as file:
        data = json.load(file)
        for resident in data['residents']:
            if resident['id'] == id:
                return resident


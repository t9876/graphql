from ariadne import QueryType, gql, make_executable_schema, ObjectType
from ariadne.asgi import GraphQL


# *************Client side*************************
# Schema Definition & Validation using gql

type_def = gql("""
        type Query{
            people : [Person!]
        }

        type Person {
            firstname: String!
            lastname: String!
            age: Int!
            fullname: String!
        }""")


# ****************** Server side logic**************************
# Instantiating query as QueryType object
query = QueryType()
@query.field("people")
def resolve_people(*_):
    print(*_)
    return [
        { "firstname": "Sourav", "lastname": "Roy", "age": 31},
        { "firstname": "Pratik", "lastname": "Kumar", "age": 24}
    ]

person = ObjectType("Person")
@person.field("fullname")
def resolve_person_fullname(person, *_):
    print(person)
    return "%s %s" % (person["firstname"], person["lastname"])


# Binding schema and making it executable
schema = make_executable_schema(type_def, query, person)
app = GraphQL(schema, debug=True)
